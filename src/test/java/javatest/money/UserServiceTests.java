package javatest.money;

import javatest.money.entity.User;
import javatest.money.exception.NotFoundException;
import javatest.money.interfaces.service.IUserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests {
    @Autowired
    private IUserService service;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void crudTest() {
        List<User> list = service.getAll();

        User user = new User();
        user.setName("test user");

        User created = service.create(user);

        List<User> list1 = service.getAll();
        assertTrue(list1.size() - list.size() == 1);
        assertTrue(created != null);
        assertTrue(created.getId() != null);

        User createdFromDb = service.getById(created.getId()); //exception if not found

        String newName = "test user 2";
        createdFromDb.setName(newName);

        User updatedUser = service.update(createdFromDb);

        assertTrue(updatedUser != null);

        User updatedUserFromDb = service.getById(updatedUser.getId());
        assertTrue(updatedUserFromDb.getName().equals(newName));

        User userByName = service.getByName(newName); //exception if not found

        service.deleteById(userByName.getId());
        List<User> list2 = service.getAll();
        assertTrue(list1.size() - list2.size() == 1);
    }

    @Test
    public void invalidIdTest() {
        exception.expect(NotFoundException.class);
        service.getById(Long.valueOf((99)));
    }

    @Test
    public void invalidNameTest() {
        exception.expect(NotFoundException.class);
        service.getByName("some non-existent name");
    }
}

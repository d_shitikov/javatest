package javatest.money;

import javatest.money.entity.Account;
import javatest.money.dto.TransferDto;
import javatest.money.entity.User;
import javatest.money.exception.BadRequestException;
import javatest.money.exception.NotFoundException;
import javatest.money.exception.NotImplementedException;
import javatest.money.interfaces.service.IAccountService;
import javatest.money.interfaces.service.IUserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTests {
    @Autowired
    private IAccountService service;

    //@MockBean
    @Autowired
    private IUserService userService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private User user;

    @Before
    public void initialize() {
        user = new User();
        user.setId(Long.valueOf(1));
        user.setName("account test");

        userService.create(user);
    }

    @Test
    public void createReadTest() {
        String accNumber = "00000000000000000099";

        List<Account> list = service.getAll();

        Account acc = new Account();
        acc.setNumber(accNumber);
        acc.setUser(user);
        acc.setAmount(new BigDecimal(500));

        Account created = service.create(acc);

        List<Account> list1 = service.getAll();
        assertTrue(list1.size() - list.size() == 1);
        assertTrue(created != null);
        assertTrue(created.getId() != null);

        Account createdFromDb = service.getById(created.getId()); //exception if not found
        assertTrue(createdFromDb != null);

        Account accByNum = service.getByNumber(accNumber); //exception if not found
        assertTrue(accByNum != null);

        List<Account> accByUserId = service.getByUserId(user.getId());
        assertTrue(accByUserId.size() == 1);

        Account accMinus = new Account();
        accMinus.setNumber("00000000000000000999");
        accMinus.setUser(user);
        accMinus.setAmount(new BigDecimal(-1000));

        exception.expect(BadRequestException.class);
        service.create(accMinus);
    }

    @Test
    public void uniqueNumberTest() {
        String accNumber = "00000000000000000899";

        Account acc = new Account();
        acc.setNumber(accNumber);
        acc.setUser(user);
        acc.setAmount(BigDecimal.ZERO);

        Account acc1 = new Account();
        acc1.setNumber(accNumber);
        acc1.setUser(user);
        acc1.setAmount(BigDecimal.ZERO);

        service.create(acc);

        exception.expect(BadRequestException.class);
        service.create(acc1);
    }

    @Test
    public void deleteTest() {
        Account acc = new Account();
        acc.setNumber("00000000000000000299");
        acc.setUser(user);
        acc.setAmount(new BigDecimal(500));

        Account acc1 = new Account();
        acc1.setNumber("00000000000000000298");
        acc1.setUser(user);
        acc1.setAmount(new BigDecimal(100));

        Account created = service.create(acc);
        Account created1 = service.create(acc1);

        List<Account> list = service.getAll();

        //empty delete ok
        service.emptyById(created1.getId());
        service.deleteById(created1.getId());

        List<Account> list1 = service.getAll();
        assertTrue(list.size() - list1.size() == 1);

        //non-empty cannot delete
        exception.expect(BadRequestException.class);
        service.deleteById(created.getId());
    }

    @Test
    public void transferTest() {
        BigDecimal firstAmount = new BigDecimal(500);
        BigDecimal secondAmount = new BigDecimal(0);
        BigDecimal transferAmount = new BigDecimal(450);

        Account from = new Account();
        from.setNumber("00000000000000000199");
        from.setUser(user);
        from.setAmount(firstAmount);

        Account createdFrom = service.create(from);

        Account to = new Account();
        to.setNumber("00000000000000000198");
        to.setUser(user);
        to.setAmount(secondAmount);

        Account createdTo = service.create(to);

        TransferDto dto = new TransferDto();
        dto.setFromAccount(createdFrom);
        dto.setToAccount(createdTo);
        dto.setAmount(transferAmount);

        service.transfer(dto);

        Account fromChanged = service.getById(createdFrom.getId());
        BigDecimal expectedFirstAmount = firstAmount.subtract(transferAmount).stripTrailingZeros();
        assertTrue(fromChanged.getAmount().stripTrailingZeros().equals(expectedFirstAmount));

        Account toChanged = service.getById(createdTo.getId());
        BigDecimal expectedSecondAmount = secondAmount.add(transferAmount).stripTrailingZeros();
        assertTrue(toChanged.getAmount().stripTrailingZeros().equals(expectedSecondAmount));

        exception.expect(BadRequestException.class);
        service.transfer(dto);
    }

    @Test
    public void invalidIdTest() {
        exception.expect(NotFoundException.class);
        service.getById(Long.valueOf((99)));
    }

    @Test
    public void invalidNumberTest() {
        exception.expect(NotFoundException.class);
        service.getByNumber("00000000000000000000");
    }

    @Test
    public void updateNotAllowedTest() {
        Account acc = new Account();
        acc.setNumber("00000000000000000098");
        acc.setUser(user);
        acc.setAmount(new BigDecimal(500));

        Account created = service.create(acc);

        Account createdFromDb = service.getById(created.getId()); //exception if not found

        exception.expect(NotImplementedException.class);
        service.update(createdFromDb);
    }
}

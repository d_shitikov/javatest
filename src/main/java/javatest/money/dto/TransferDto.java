package javatest.money.dto;

import javatest.money.entity.Account;

import java.math.BigDecimal;

public class TransferDto {
    private Account FromAccount;

    private Account ToAccount;

    private BigDecimal Amount;

    public Account getFromAccount() {
        return FromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        FromAccount = fromAccount;
    }

    public Account getToAccount() {
        return ToAccount;
    }

    public void setToAccount(Account toAccount) {
        ToAccount = toAccount;
    }

    public BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(BigDecimal amount) {
        Amount = amount;
    }
}

package javatest.money.service;

import javatest.money.entity.User;
import javatest.money.exception.BadRequestException;
import javatest.money.exception.NotFoundException;
import javatest.money.interfaces.repository.IUserRepository;
import javatest.money.interfaces.service.IUserService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {
    @Autowired
    private IUserRepository repository;

    @Override
    public User getById(Long id) throws NotFoundException {
        Optional<User> user =  repository.findById(id);

        if (!user.isPresent()) {
            throw new NotFoundException("user with id [" + id + "] not found");
        }

        return user.get();
    }

    @Override
    public User getByName(String name) throws NotFoundException {
        User user = repository.findByName(name);

        if (user == null) {
            throw new NotFoundException("user with name [" + name + "] not found");
        }

        return user;
    }

    @Override
    public User create(User input) {
        input.setId(null);
        return repository.save(input);
    }

    @Override
    public User update(User input) throws NotFoundException {
        if (!repository.existsById(input.getId())) {
            throw new NotFoundException("user with id [" + input.getId() + "] not found");
        }

        return repository.save(input);
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }

    @Override
    public void deleteById(Long id) throws BadRequestException {
        User user = getById(id);

        try {
            repository.delete(user);
        } catch (DataIntegrityViolationException e) {
            Throwable th = e.getCause();
            if (th instanceof ConstraintViolationException) {
                throw new BadRequestException("cannot delete user with accounts");
            }
            throw e;
        }
    }
}

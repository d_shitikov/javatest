package javatest.money.service;

import javatest.money.entity.Account;
import javatest.money.dto.TransferDto;
import javatest.money.exception.BadRequestException;
import javatest.money.exception.NotFoundException;
import javatest.money.exception.NotImplementedException;
import javatest.money.interfaces.repository.IAccountRepository;
import javatest.money.interfaces.service.IAccountService;
import javatest.money.interfaces.service.IUserService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService implements IAccountService {
    private static final Logger log = LogManager.getRootLogger();

    @Autowired
    private IAccountRepository repository;

//    @Autowired
//    private IUserService userService;

    @Override
    public List<Account> getAll() {
        return repository.findAll();
    }

    @Override
    public Account getById(Long id) throws NotFoundException {
        Optional<Account> account =  repository.findById(id);

        if (!account.isPresent()) {
            throw new NotFoundException("account with id [" + id + "] not found");
        }

        return account.get();
    }

    @Override
    public Account getByNumber(String number) throws NotFoundException {
        Account account = repository.findByNumber(number);

        if (account == null) {
            throw new NotFoundException("account with number [" + number + "] not found");
        }

        return account;
    }

    @Override
    public List<Account> getByUserId(Long userId) {
        List<Account> accounts = repository.findByUserId(userId);

        return accounts;
    }

    @Override
    public Account create(Account input) throws BadRequestException {
        input.setId(null);

        if (input.getAmount().compareTo(BigDecimal.ZERO) == -1) {
            throw new BadRequestException("amount must be positive or zero");
        }

        //userService.getById(input.getUserId()); //exception if not found

        Account saved;
        try {
            saved = repository.save(input);
        } catch (DataIntegrityViolationException e) {
            Throwable th = e.getCause();
            if (th instanceof ConstraintViolationException) {
                throw new BadRequestException("account number [" + input.getNumber() + "] already exist or user with id [" + input.getUser().getId() + "] not exist");
            }
            throw e;
        }

        log.info("successful creation of the account [{}] for userId [{}] with amount [{}]", input.getNumber(), input.getUser().getId(), input.getAmount());

        return saved;
    }

    @Override
    public Account update(Account input) throws NotImplementedException {
        throw new NotImplementedException("direct account update not allowed");
    }

    @Override
    public void deleteById(Long id) throws BadRequestException {
        Account account = getById(id);

        //amount > 0
        if (account.getAmount().compareTo(new BigDecimal(0)) > 0) {
            throw new BadRequestException("account amount must be zero to allow delete");
        }

        repository.delete(account);
    }

    @Override
    public void emptyById(Long id) {
        Account account = getById(id);

        account.setAmount(new BigDecimal(0));

        repository.save(account);

        log.info("successful empty: account [{}]", account.getNumber());
    }

    @Override
    @Transactional
    public void transfer(TransferDto dto) throws BadRequestException {
        if (dto.getFromAccount() == null) {
            throw new BadRequestException("fromAccount is null");
        }

        if (dto.getFromAccount().getId() == null) {
            throw new BadRequestException("fromAccount id is null");
        }

        if (dto.getToAccount() == null) {
            throw new BadRequestException("toAccount is null");
        }

        if (dto.getToAccount().getId() == null) {
            throw new BadRequestException("toAccount id is null");
        }

        Long fromId = dto.getFromAccount().getId();
        Long toId = dto.getToAccount().getId();

        Account accountFrom = getById(fromId);

        Account accountTo = getById(toId);

        //amount < account amount
        if (accountFrom.getAmount().compareTo(dto.getAmount()) < 0) {
            throw new BadRequestException("accountFrom has no sufficient money amount");
        }

        BigDecimal amountFrom = accountFrom.getAmount().subtract(dto.getAmount());
        accountFrom.setAmount(amountFrom);
        repository.save(accountFrom);

        //transaction check
        //userService.getById(Long.valueOf(99)); //exception 404

        BigDecimal amountTo = accountTo.getAmount().add(dto.getAmount());
        accountTo.setAmount(amountTo);
        repository.save(accountTo);

        log.info("successful transfer: from account [{}] to account [{}], amount [{}]", accountFrom.getNumber(), accountTo.getNumber(), dto.getAmount());
    }
}

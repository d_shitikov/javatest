package javatest.money.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "ACCOUNT")
public class Account {
    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;
    
    @Column(name = "NUMBER", length = 20, unique = true, nullable = false)
    @Size(min = 20, max = 20)
    private String number;

    //@Column(name = "USER_ID", nullable = false)
    //private Long userId;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount = new BigDecimal(0.00);

    @ManyToOne (fetch = FetchType.EAGER, optional=false)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

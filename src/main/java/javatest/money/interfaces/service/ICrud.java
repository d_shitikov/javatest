package javatest.money.interfaces.service;

import java.util.List;

public interface ICrud<T> {
    T getById(Long id);

    List<T> getAll();

    T create(T input);

    T update(T input);

    void deleteById(Long id);
}

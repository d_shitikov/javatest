package javatest.money.interfaces.service;

import javatest.money.entity.User;

public interface IUserService extends ICrud<User> {
    User getByName(String name);
}

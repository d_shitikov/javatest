package javatest.money.interfaces.service;

import javatest.money.dto.TransferDto;

public interface IMoneyManagement {
    void emptyById(Long id);

    void transfer(TransferDto dto);
}

package javatest.money.interfaces.service;

import javatest.money.entity.Account;

import java.util.List;

public interface IAccountService extends ICrud<Account>, IMoneyManagement {
    Account getByNumber(String number);

    List<Account> getByUserId(Long userId);
}

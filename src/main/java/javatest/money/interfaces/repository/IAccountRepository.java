package javatest.money.interfaces.repository;

import javatest.money.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface IAccountRepository extends JpaRepository<Account, Long> {
    @Query("select a from Account a where a.number = :number")
    Account findByNumber(@Param("number") String number);

    @Query("select a from Account a where a.user.id = :userId")
    List<Account> findByUserId(@Param("userId") Long userId);
}

package javatest.money.controller;

import javatest.money.entity.User;
import javatest.money.interfaces.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private IUserService service;

    @GetMapping
    public List<User> list() {
        return service.getAll();
    }

    @GetMapping("by_name/{name}")
    public User findByName(@PathVariable String name) {
        return service.getByName(name);
    }

    @GetMapping("{id}")
    public User getOne(@PathVariable Long id) {
        return service.getById(id);
    }

    @PostMapping
    public User create(@RequestBody User user) {
        return service.create(user);
    }

    @PutMapping
    public User update(@RequestBody User user) {
        return service.update(user);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id)
    {
        service.deleteById(id);
    }
}

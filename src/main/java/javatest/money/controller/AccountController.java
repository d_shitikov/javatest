package javatest.money.controller;

import javatest.money.entity.Account;
import javatest.money.dto.TransferDto;
import javatest.money.interfaces.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("account")
public class AccountController {
    @Autowired
    private IAccountService service;

    @GetMapping
    public List<Account> list() {
        return service.getAll();
    }

    @GetMapping("{id}")
    public Account getOne(@PathVariable Long id)
    {
        return service.getById(id);
    }

    @GetMapping("by_number/{number}")
    public Account findByNumber(@PathVariable String number) {
        return service.getByNumber(number);
    }

    @GetMapping("by_user_id/{id}")
    public List<Account> findByUserId(@PathVariable Long id) {
        return service.getByUserId(id);
    }

    @PostMapping
    public Account create(@RequestBody Account account) {
        return service.create(account);
    }

    @PutMapping
    public Account update(@RequestBody Account account) {
        return service.update(account);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }

    @PostMapping("empty/{id}")
    public void emptyById(@PathVariable Long id) {
        service.emptyById(id);
    }

    @PostMapping("transfer")
    public void transfer(@RequestBody TransferDto dto) {
        service.transfer(dto);
    }
}
